/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oka88.config;

/**
 *
 * @author daua1993
 */
public class Common {

    public static final String CONFIG_NAME = "WEB-LANDING";
    public static final String BASE_FOLDER = System.getProperty("user.home") + java.io.File.separator;
    public final static String CONFIG_FOLDER = BASE_FOLDER + "config" + java.io.File.separator + "web-landing" + java.io.File.separator;
    public static final String CONFIG_FILE_NAME = "appSettings.xml";

    public final static String BANKING_CDR = "BANKING_CDR";

    public static final int PARTNER_SELL_GOLD_TO_PARTNER = 5;
    public static final int PARTNER_PARENT_TRANSFER_TO_CHILD = 6;
    public static final int PARTNER_CHILD_TRANSFER_TO_PARENT = 7;

}
