package com.oka88.config;

import com.google.common.base.MoreObjects;
import javax.servlet.http.HttpServletRequest;

public class AppUtils {

    public static String getIp(HttpServletRequest request) {
        return MoreObjects.firstNonNull(request.getHeader("X-FORWARDED-FOR"), MoreObjects.firstNonNull(request.getHeader("x-forwarded-for"), request.getRemoteAddr()));
    }

    public static String parseString(Object obj) {
        if (obj == null) {
            return "";
        }
        try {
            return String.valueOf(obj);
        } catch (Exception ex) {
        }
        return "";
    }

    public static long parseLong(Object o) {
        if (o == null) {
            return 0;
        }
        if (o instanceof Double) {
            return ((Double) o).longValue();
        }
        if (o instanceof Float) {
            return ((Float) o).longValue();
        }
        try {
            return Long.parseLong(String.valueOf(o));
        } catch (Exception e) {
        }
        return 0;
    }

    public static int parseInt(Object o) {
        if (o == null) {
            return 0;
        }
        if (o instanceof Double) {
            return ((Double) o).intValue();
        }
        if (o instanceof Float) {
            return ((Float) o).intValue();
        }
        try {
            return Integer.parseInt(String.valueOf(o));
        } catch (Exception e) {
        }
        return 0;
    }

    public static String getProductName() {
        return AppConfig.getConfig().getProperty("PRODUCT_NAME", "GemVip - Vương quốc Gem", "SETTINGS");
    }

}
