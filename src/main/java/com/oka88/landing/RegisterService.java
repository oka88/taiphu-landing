package com.oka88.landing;

import com.google.common.base.Strings;
import com.oka88.config.AppConfig;
import com.oka88.config.AppUtils;
import com.oka88.http.HttpSender;
import com.oka88.http.HttpSenderImpl;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author daua1993
 */
@WebServlet(name = "register", urlPatterns = {"/register"})
public class RegisterService extends HttpServlet {

    private final Logger logger = LoggerFactory.getLogger("RegisterService");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");

        String captcha = request.getParameter("captcha");
        String tokenCaptra = request.getParameter("token");

        String cp = request.getParameter("cp");
        String ref = request.getParameter("ref");
        String page = request.getParameter("page");

        int maintain = AppConfig.getConfig().getIntProperty("MAIN_TAIN", 0, "SETTINGS");
        if (maintain == 1) {
            response.sendRedirect(page);
            return;
        }

        if (Strings.isNullOrEmpty(page)) {
            response.sendRedirect(page);
            return;
        }

        HttpSender sender = new HttpSenderImpl();
        JSONObject params = new JSONObject();
        params.put("username", username);
        params.put("display_name", "");
        params.put("password", password);
        params.put("confirm_password", repassword);
        params.put("ip", AppUtils.getIp(request));
        params.put("cp", cp);
        params.put("ref", ref);

        params.put("captcha", captcha);
        params.put("token_captcha", tokenCaptra);

        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-type", "application/json");

        char p = page.charAt(page.length() - 1);
        String wc = "welcome" + String.valueOf(p);
        logger.info("welcome-page = {}", wc);

        String url = AppConfig.getConfig().getProperty("URL_REGISTER", "http://103.71.252.250:20989/user-statistic/auth/register", "SETTINGS");
//        url = "http://103.71.252.250:20989/user-statistic/auth/register";
        JSONObject result = sender.postJson(url, headers, params);
        logger.info("=>register: url={}, params={}", url, params);
        if (result != null) {
            logger.info("result: " + result.toJSONString());
            int rc = AppUtils.parseInt(result.get("rc"));
            if (rc == 0) {
                String token = AppUtils.parseString(result.get("tk"));
                request.setAttribute("token", token);
                request.setAttribute("username", username);
                HttpSession session = request.getSession();
                session.setAttribute("token", token);
                session.setAttribute("username", username);
                wc += "?t=" + token + "&u=" + username;
                response.sendRedirect(wc);
            } else {
                String rd = AppUtils.parseString(result.get("rd"));
                request.setAttribute("error", rd);
                request.setAttribute("username", username);
                request.getRequestDispatcher(page).forward(request, response);
            }
        } else {
            System.out.println("result: null");
            request.setAttribute("error", "Đăng ký thất bại, vui lòng thử lại");
            request.getRequestDispatcher(page).forward(request, response);
        }

    }

}
