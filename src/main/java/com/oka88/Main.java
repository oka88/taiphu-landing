package com.oka88;

import com.oka88.config.AppConfig;
import com.oka88.config.Common;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.logging.log4j.core.config.Configurator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
        Configurator.initialize(null, Common.CONFIG_FOLDER + "log4j2.xml");
        Logger logger = LoggerFactory.getLogger(this.getClass());
        try {
            logger.info("server inited !!");
            logger.info("===============!");
            logger.info("init server !!!!");
            logger.info("CONFIG_FOLDER: = {}", Common.CONFIG_FOLDER);
            AppConfig appConfig = new AppConfig();
            appConfig.reloadConfig();
            AppConfig.getConfig().getProperty("", "");
        } catch (Exception ex) {
            logger.error("Load Config Failed {}", ex);
        }
    }

}
