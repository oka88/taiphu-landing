/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oka88.http;

import com.google.common.base.Strings;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author daua1993
 */
public class HttpSenderImpl extends HttpSender {

//    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static CloseableHttpClient httpclientScratch;

    private synchronized static CloseableHttpClient getHttpClientScrachtInstance() {
        if (httpclientScratch == null) {
            int timeout = 30000;
            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(timeout)
                    .setConnectTimeout(timeout)
                    .setConnectionRequestTimeout(timeout)
                    .build();
            httpclientScratch = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        }
        return httpclientScratch;
    }

    @Override
    public JSONObject postJson(String uri, Map<String, String> header, JSONObject params) {
        JSONObject result = null;
        String rs;
        try {
            HttpPost post = new HttpPost(uri);
            if (header != null && !header.isEmpty()) {
                for (Map.Entry<String, String> entry : header.entrySet()) {
                    post.addHeader(entry.getKey(), entry.getValue());
                }
            }
            StringEntity reqEntity = new StringEntity(params.toString(), StandardCharsets.UTF_8);
            post.setEntity(reqEntity);
            CloseableHttpResponse response = getHttpClientScrachtInstance().execute(post);
            HttpEntity entity = response.getEntity();
            rs = getStringResponse(entity);
//            logger.info("postJson result: {}", rs);
            if (!Strings.isNullOrEmpty(rs)) {
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            logger.error("Exception: ", ex);
        }

        return result;
    }

    private String getStringResponse(HttpEntity entity) throws IOException {
        String rs;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent(), StandardCharsets.UTF_8))) {
            char[] tmp = new char[1024];
            StringBuilder sb = new StringBuilder();
            int readBytes;
            while ((readBytes = br.read(tmp)) != -1) {
                sb.append(tmp, 0, readBytes);
            }
            rs = sb.toString();
        }
        return rs;
    }

    @Override
    public JSONObject postForm(String uri, Map<String, String> header, Map<String, String> params) {
        JSONObject result = null;
        String rs;
        try {
            HttpPost post = new HttpPost(uri);
            List<NameValuePair> nvp = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                nvp.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            post.setEntity(new UrlEncodedFormEntity(nvp, StandardCharsets.UTF_8));
            CloseableHttpResponse response = getHttpClientScrachtInstance().execute(post);
            HttpEntity entity = response.getEntity();
            rs = getStringResponse(entity);
//            logger.info("postForm result: {}", rs);
            if (!Strings.isNullOrEmpty(rs)) {
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            logger.error("Exception: ", ex);
        }
        return result;
    }

    @Override
    public JSONObject get(String url, Map<String, String> header, Map<String, String> params) {
        JSONObject result = null;
        String rs = "";
        try {
            HttpGet httpGet = new HttpGet(url);
            if (header != null && !header.isEmpty()) {
                for (Map.Entry<String, String> entry : header.entrySet()) {
                    httpGet.addHeader(entry.getKey(), entry.getValue());
                }
            }
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            if (params != null && !params.isEmpty()) {
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    NameValuePair nv2 = new BasicNameValuePair(entry.getKey(), entry.getValue());
                    nameValuePairs.add(nv2);
                }
            }
            if (!nameValuePairs.isEmpty()) {
                URI uri = new URIBuilder(httpGet.getURI()).addParameters(nameValuePairs).build();
                httpGet.setURI(uri);
            }
//            logger.info(httpGet.getURI().toString());
            CloseableHttpResponse response = getHttpClientScrachtInstance().execute(httpGet);
            HttpEntity entity = response.getEntity();
            rs = getStringResponse(entity);
            if (!Strings.isNullOrEmpty(rs)) {
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            logger.error("Exception: ", ex);
        }
        return result;
    }
}
