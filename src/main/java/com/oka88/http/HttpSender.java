/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oka88.http;

import java.util.Map;
import org.json.simple.JSONObject;

/**
 *
 * @author daua1993
 */
public abstract class HttpSender {

    public abstract JSONObject postJson(String uri, Map<String, String> header, JSONObject params);

    public abstract JSONObject postForm(String uri, Map<String, String> header, Map<String, String> params);

    public abstract JSONObject get(String uri, Map<String, String> header, Map<String, String> params);
}
