<%-- 
    Document   : welcome3
    Created on : Feb 11, 2019, 4:22:41 PM
    Author     : daua1993
--%>


<%@page import="com.oka88.config.AppUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<%=request.getContextPath()%>/images/icon.png" type="image/x-icon">
        <title><%=AppUtils.getProductName()%></title>
        <style type="text/css">


            *{margin: 0;padding: 0;}
            body{background: url(<%=request.getContextPath()%>/welcome/bg.jpg) no-repeat top center;background-color:#62110f;}
            a.button{display: block;width: 442px;height: 140px;margin: 320px auto 0;background: url(<%=request.getContextPath()%>/welcome/btn-thamgiangay.png) no-repeat top center;text-indent: -9999px;-webkit-animation-name: button;
                     animation-name: button;
                     -webkit-animation-duration: 0.3s;
                     animation-duration: 0.3s;
                     -webkit-animation-iteration-count: infinite;
                     animation-iteration-count: infinite;
                     -webkit-animation-direction: alternate;
                     animation-direction: alternate}
            h3{text-align:center;color:#fff;font-family:Roboto, Arial, Sans-serif;font-size:32px;text-shadow:1px 2px 8px #000;}

            @-webkit-keyframes button {
                from {
                    -webkit-transform: scale(1);
                    transform: scale(1);
                }

                to {
                    -webkit-transform: scale(1.1);
                    transform: scale(1.1);
                }
            }

            @keyframes button {
                from {
                    -webkit-transform: scale(1);
                    transform: scale(1);
                }

                to {
                    -webkit-transform: scale(1.1);
                    transform: scale(1.1);
                }
            }

        </style>
    </head>

    <body onload="onLoad();">

        <a href="goto-game" class="button">Tham gia ngay</a>
        <h3 id="redirect">Hệ thống sẽ tự động chuyển sang trang chơi game sau <font color="yellow">5</font> giây</h3>

        <script type="text/javascript">
            function onLoad() {
                var start = 5;
                var timer = setInterval(() => {
                    console.log("t = " + start);
                    var t = '<h3 id="redirect">Hệ thống sẽ tự động chuyển sang trang chơi game sau <font color="yellow">' + start + '</font> giây</h3>';
                    document.getElementById("redirect").innerHTML = t;
                    start--;
                    if (start < 0) {
                        clearInterval(timer);
                        window.location.href = "goto-game";
                    }
                }, 1000);
            }
        </script>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PVQDQ29');</script>
        <!-- End Google Tag Manager -->

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PVQDQ29" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

    </body>

</html>

