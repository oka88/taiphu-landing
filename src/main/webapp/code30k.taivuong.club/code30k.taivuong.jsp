<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="vi">

    <head>
        <meta charset="utf-8">
        <meta name="google" content="notranslate" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=no">

        <link rel="icon" href="favi.png" type="image/x-icon">
        <meta name="conf-site-verification" content="EGj71V6nUpsZ74a4OULjCSaXOzuZOfrFriCFtMDqBtiAw16WvPRZGKNvGBXsL5qCou38zWlI++6wxOXwYnhMHxK9me8cr/Z5bQwiTvPalBmHQSOPeQLmcRhNZ7daZlnYbzyNwp99l6REanEWbCIfTslejBkmDlmG3zPOCglVbfznLa2fE1iiMf6JFJxa4/nZNQwmFzH6GfGJ5SRkm5Vxoo+9bZupM/mAGS4UqdKQwO1IEuSneYpuDBNne+5xPamX"
              />
        <title>Chơi Vương, Sướng Như Vua</title>
        <link href="<%=request.getContextPath()%>/code30k.taivuong.club/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/code30k.taivuong.club/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,400,700,900" rel="stylesheet" type="text/css">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link href="<%=request.getContextPath()%>/code30k.taivuong.club/build/style.min.css" rel="stylesheet">

    </head>

    <body>

        <div class="wrapall">
            <h1>Vương Club Là cỗng game bài đổi thưởng với số lượng người chơi đông nhất 2020 tại Việt Nam và Châu Á với nhiều trò chơi hấp dẫn</h1>
            <header>
                <div class="container">
                    <div class="row">
                        <a href="/" class="logo text-center">
                            <img class="img-responsive" data-src="<%=request.getContextPath()%>/code30k.taivuong.club/images/logo.png?v=3" src="<%=request.getContextPath()%>/code30k.taivuong.club/images/logo-lazy.png?v=3" alt="MAYCLUB - Game đánh bài đổi thưởng uy tín 2020">
                        </a>
                    </div>
                    <div class="row">
                        <a href="/" class="slogan text-center">
                            <img class="img-responsive" data-src="<%=request.getContextPath()%>/code30k.taivuong.club/images/slogan.png?v=4" src="<%=request.getContextPath()%>/code30k.taivuong.club/images/slogan-lazy.png?v=3" alt="MAYCLUB - Game đổi thẻ uy tín, chơi và tải game bài online">
                        </a>
                    </div>
                </div>
            </header>
            <section id="form">
                <div class="container">
                    <div class="row">
                        <ul class="nav nav-tabs">
                            <li class="active"><a id="register-tab" class="show active" data-toggle="tab" href="#register">Đăng ký</a></li>
                            <li id="choinhanhtab"><a id="login-tab" data-toggle="tab" href="#login">Đăng nhập</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="register" class="tab-pane fade">
                                <form class="regFrm" role="form" name="registerForm" action="" id="registerForm" method="POST">
                                    <div class="form-group">
                                        <i class="fas fa-user icon-prepend"></i>
                                        <input type="text" class="form-control" id="usrname" name="username" placeholder="Tên đăng nhập">
                                        <small id="errUsrname" class="form-text text-danger d-none">Bạn phải nhập tên đăng nhập.</small>
                                    </div>
                                    <div class="form-group">
                                        <i class="fas fa-lock icon-prepend"></i>
                                        <input type="password" class="form-control" id="pwd" name="password" placeholder="Mật khẩu">
                                        <i class="far fa-eye"></i>
                                        <small id="errPwd" class="form-text text-danger d-none">Bạn phải nhập mật khẩu.</small>
                                    </div>
                                    <div class="form-group">
                                        <i class="fas fa-lock icon-prepend"></i>
                                        <input type="password" class="form-control" id="repeat-pwd" name="repeat_pwd" placeholder="Nhập lại mật khẩu">
                                        <i class="far fa-eye"></i>
                                        <small id="errRepeatPwd" class="form-text text-danger d-none">Xác nhận mật khẩu không đúng.</small>
                                    </div>
                                    <div class="form-group mb-3">
                                        <div class="capcha d-block clearfix">
                                            <img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/capcha.jpg" alt="" class="imgcapcha"/>
                                            <input type="text" class="form-control col-8" name="repeat_pwd" placeholder="Nhập Captcha">
                                        </div>
                                        <small id="" class="form-text text-danger d-none">Captcha  không đúng.</small>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="store btnsubmit">
                                            <img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/btn-dangky.png?v=1" alt="Hướng dẫn game bài đổi thưởng uy tín nhất"  width="100%" />
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div id="login" class="tab-pane fade">
                                <form class="loginFrm" role="form" name="loginForm" action="" id="loginForm" method="POST">
                                    <div class="form-group">
                                        <i class="fas fa-user icon-prepend"></i>
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Tên đăng nhập">
                                    </div>
                                    <div class="form-group mb-1">
                                        <i class="fas fa-lock icon-prepend"></i>
                                        <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
                                        <i class="far fa-eye"></i>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="store btnsubmit">
                                            <img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/btn-dangnhap.png?v=1" alt="Đăng nhập cổng game May Club trên ios" width="100%">
                                        </button>       
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="thankform">
                <div class="container text-center">
                    <div class="thankinfo">
                        <p class="thankinfo_lbl"><img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/lable-thank.png" alt="Chúc bạn may mắn nỗ hủ đều tay " width="150"></p>
                        <p class="thankinfo_sttl"><img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/title-thank.png" alt="bạn đã đăng ký thành công cổng game May Club" width="160"></p>
                        <p class="thankinfo_thanhcong">Bạn đã đăng kí thành công:</p>
                        <p class="thankinfo_user">Tên đăng nhập: abcxyz</p>
                        <p class="thankinfo_pass">Mật khẩu: ***Vic</p>
                    </div>
                </div>
            </section>
            <!-- DOWNLOAD -->
            <section id="main">
                <div class="container text-center">
                    <div class="dacotaikhoan">
                        <img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/dacotaikhoan.png" alt="game đánh bài tiến lên miền nam đổi thưởng là game có lượng người chơi đông nhất" border="0">
                    </div>
                    <div id="android" class="row">
                        <a id="taiggplay" href="javascript:;" onclick="onDownloadAndroid()" name="MAYCLUB - Game bài an toàn, đổi thưởng, thẻ cào, ăn tiền thật" class="store btn-store">
                            <img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/btn-android.png?v=1" alt="Vì sao May Club game bài đổi thưởng nhiều người chơi nhất hiện nay"  width="100%" />
                        </a>
                        <small>Bản cài đặt đã đổi tên nhằm vượt qua sự kiểm duyệt.</small>
                    </div>
                    <div id="ios" class="row">
                        <a id="taiios" href="javascript:;" onclick="onDownloadIOS();" name="MAYCLUB - Game bài an toàn, đổi thưởng, thẻ cào, ăn tiền thật" class="store btn-store">
                            <img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/btn-ios.png?v=1" alt="Tải bản cài đặt cho IOS, tận hưởng thế giới game mới"  width="100%" />
                        </a>
                        <small>Bản cài đặt đã đổi tên nhằm vượt qua sự kiểm duyệt.</small>
                    </div>
                </div>
            </section>

            <footer><img src="<%=request.getContextPath()%>/code30k.taivuong.club/images/bg-footer.jpeg" alt="Cám ơn bạn đã tham gia trò chơi đổi thưởng May Club"></footer>
            <div class="loading">
                <img alt="Chơi bài online thưởng lớn chưa từng có" src="<%=request.getContextPath()%>/code30k.taivuong.club/images/loading.gif">
            </div>
            <div id="modalbrowser" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="modal-body text-center text-danger">Vui lòng sử dụng trình duyệt Safari để tải bản cài đặt.</div>
                    </div>
                </div>
            </div>
            <!-- Alert Modal -->
            <div id="alertModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title alert">LỖI</h4>
                        </div>
                        <div class="modal-body text-center text-danger"></div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var isCHPlay = true;
            var isAppStore = true;
            var isApk = false;
            var isIpa = false;
            var isUdid = false;
            var isAction = 1;
        </script>
        <script>
            var v = '2.0.1';
        </script>
        <script src="<%=request.getContextPath()%>/code30k.taivuong.club/build/app.min.js"></script>

    </body>

</html>