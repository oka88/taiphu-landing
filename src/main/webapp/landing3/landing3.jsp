<%-- 
    Document   : landing3
    Created on : Jan 18, 2019, 12:30:18 AM
    Author     : daua1993
--%>

<%@page import="com.oka88.config.AppUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<%=request.getContextPath()%>/images/icon.png" type="image/x-icon">
        <link href="<%=request.getContextPath()%>/landing3/css/landing.css" rel="stylesheet" type="text/css">
        <title><%=AppUtils.getProductName()%></title>
        <style type="text/css">
            #btn_submit {
                background : url("<%=request.getContextPath()%>/landing3/images/btn-dangky.png");
                cursor: pointer
            }
            .not{
                color: yellow;
                font-size: 12px

            }
        </style>

    </head>

    <body onload="onLoad();">

        <div class="content">
            <form method="post" onsubmit="return validateForm()" action="register">
                <input type="hidden" id="custId" name="page" value="landing3">
                <div class="dangkyblock">
                    <ul class="dangkyform">
                        <li>
                            <input id="username" name="username" maxlength="15" onfocuso="" value="<%= request.getAttribute("username") != null ? request.getAttribute("username") : ""%>" type="text" class="input_name" placeholder="Tên đăng nhập" title="Tên tài khoản chỉ gồm chữ cái và số. Tối thiểu 6 ký tự viết liền không dấu.">
                            <em id="vl_username" class="not"><%= request.getAttribute("error") != null ? request.getAttribute("error") : ""%></em>
                        </li>
                        <li>
                            <input id="display_name" name="display_name" type="text" placeholder="Tên nhân vật trong game" title="Tên nhân vật không được trùng với tên tài khoản. Tối thiểu 6 ký tự gồm chữ và số viết liền không dấu!">
                            <em id="vl_display" class="not"></em>
                        </li>
                        <li>
                            <input id="password" name="password" type="password" class="input_pass" placeholder="Mật khẩu" title="">
                            <em id="vl_pass" class="not"></em>
                        </li>
                        <li>
                            <input id="repassword" name="repassword" type="password" class="input_pass" placeholder="Nhập lại mật khẩu" title="">
                            <em id="vl_repass" class="not"></em>
                        </li>
                        <li>
                            <input type="submit" class="buttondangky" id="btn_submit" value="" >
                        </li>
                    </ul>
                </div>
            </form>
        </div>
        <div class="footer">

        </div>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PVQDQ29');</script>
        <!-- End Google Tag Manager -->
        Additionally, paste this code immediately after the opening <body> tag:

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PVQDQ29"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <script type="text/javascript">

            function onLoad() {
                document.getElementById("username").focus();
                var username = document.getElementById("username").value;
                document.getElementById("username").value = "";
                document.getElementById("username").value = username;
            }

            function validateForm() {

                document.getElementById("vl_username").textContent = "";
                document.getElementById("vl_display").textContent = "";
                document.getElementById("vl_pass").textContent = "";
                document.getElementById("vl_repass").textContent = "";

                var username = document.getElementById("username").value;
                var display_name = document.getElementById("display_name").value;
                var password = document.getElementById("password").value;
                var repassword = document.getElementById("repassword").value;

                var val = validateUsername("Tên đăng nhập", username);
                if (val.length !== 0) {
                    document.getElementById("vl_username").textContent = val;
                    document.getElementById("username").focus();
                    return false;
                }

                val = validateUsername("Tên nhân vật", display_name);
                if (val.length !== 0) {
                    document.getElementById("vl_display").textContent = val;
                    document.getElementById("display_name").focus();
                    return false;
                }

                if (password.length < 6) {
                    document.getElementById("vl_pass").textContent = "Vui lòng nhập mật khẩu từ 6 ký tự trở lên";
                    document.getElementById("password").focus();
                    return false;
                }

                if (repassword.length === 0) {
                    document.getElementById("vl_pass").textContent = "Vui lòng nhập lại mật khẩu";
                    document.getElementById("password").focus();
                    return false;
                }

                if (password !== repassword) {
                    document.getElementById("vl_repass").textContent = "Nhập lại mật khẩu không đúng";
                    document.getElementById("repassword").focus();
                    return false;
                }

                return true;

            }

            function validateUsername(name, str) {
                var error = "";
                var illegalChars = /\W/;
                if (str === "") {
                    error = "Vui lòng nhập " + name;
                } else if ((str.length < 6) || (str.length > 15)) {
                    error = name + " phải từ 6 - 15 ký tự";
                } else if (illegalChars.test(str)) {
                    error = name + " không đúng định dạng";
                } else {
                    error = "";
                }
                return error;
            }

        </script>

    </body>
</html>

