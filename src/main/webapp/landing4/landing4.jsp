<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>GemVip</title>
        <link rel="icon" href="<%=request.getContextPath()%>/images/icon.png" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto:700,900" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/landing4/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/landing4/assets/css/taiphu.css">
        <style type="text/css">
            #btn_submit {
                background : url("<%=request.getContextPath()%>/landing4/assets/images/btn_DK.png");
                cursor: pointer
            }
            .not{
                color: yellow;
                font-size: 12px
            }
        </style>
    </head>
    <body class="landing2" onload="onLoad();">
        <div id="coins"></div>
        <section id="box-main">
            <article id="box-text">
                <div class="logo">
                    <img src="<%=request.getContextPath()%>/landing4/assets/images/logo.png" class="bounce-top" alt="logo-bomtan">
                </div>
                <div id="text-img">
                    <img src="<%=request.getContextPath()%>/landing4/assets/images/text1.png" alt="">
                </div>
            </article>
            <article id="box-gift">
                <div id="girl">
                    <img src="<%=request.getContextPath()%>/landing4/assets/images/girl1.png" alt="">
                </div>
                <div id="gift">
                    <img src="<%=request.getContextPath()%>/landing4/assets/images/oto-mb.png" class="hide-desktop shake-horizontal" alt="">
                    <img src="<%=request.getContextPath()%>/landing4/assets/images/oto.png" class="shake-horizontal hide-mobile" alt="">
                </div>
                <div id="form">
                    <form class="form-regist" method="post" onsubmit="return validateForm()" action="register"> 
                        <h4 class="form-header text-uppercase text-center">Đăng ký nhanh</h4>
                        <input type="hidden" id="custId" name="page" value="landing4">
                        <div id="content-form">
                            <div class="form-group">
                                <input type="text" id="username" name="username" autocomplete="off" class="form-control" placeholder="Tên đăng nhập">
                                <em id="vl_username" class="not"><%= request.getAttribute("error") != null ? request.getAttribute("error") : ""%></em>
                            </div>
                            <div class="form-group">
                                <input type="text" id="display_name" name="display_name" autocomplete="off" class="form-control" placeholder="Tên nhân vật trong game">
                                <em id="vl_display" class="not"></em>
                            </div>
                            <div class="form-group">
                                <input id="password" name="password"  type="password"autocomplete="off"class="form-control" placeholder="Mật khẩu">	
                                <em id="vl_pass" class="not"></em>
                            </div>
                            <div class="form-group">
                                <input type="password" id="repassword" name="repassword" class="form-control" placeholder="Nhập lại mật khẩu">	
                                <em id="vl_repass" class="not"></em>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn-regist">
                                    <img src="<%=request.getContextPath()%>/landing4/assets/images/btn_DK.png" alt="">	
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </article>
        </section>			

        <script type="text/javascript">

            function onLoad() {
                document.getElementById("username").focus();
                var username = document.getElementById("username").value;
                document.getElementById("username").value = "";
                document.getElementById("username").value = username;
            }

            function validateForm() {

                document.getElementById("vl_username").textContent = "";
                document.getElementById("vl_display").textContent = "";
                document.getElementById("vl_pass").textContent = "";
                document.getElementById("vl_repass").textContent = "";

                var username = document.getElementById("username").value;
                var display_name = document.getElementById("display_name").value;
                var password = document.getElementById("password").value;
                var repassword = document.getElementById("repassword").value;

                var val = validateUsername("Tên đăng nhập", username);
                if (val.length !== 0) {
                    document.getElementById("vl_username").textContent = val;
                    document.getElementById("username").focus();
                    return false;
                }

                val = validateUsername("Tên nhân vật", display_name);
                if (val.length !== 0) {
                    document.getElementById("vl_display").textContent = val;
                    document.getElementById("display_name").focus();
                    return false;
                }

                if (password.length < 6) {
                    document.getElementById("vl_pass").textContent = "Vui lòng nhập mật khẩu từ 6 ký tự trở lên";
                    document.getElementById("password").focus();
                    return false;
                }

                if (repassword.length === 0) {
                    document.getElementById("vl_pass").textContent = "Vui lòng nhập lại mật khẩu";
                    document.getElementById("password").focus();
                    return false;
                }

                if (password !== repassword) {
                    document.getElementById("vl_repass").textContent = "Nhập lại mật khẩu không đúng";
                    document.getElementById("repassword").focus();
                    return false;
                }

                return true;

            }

            function validateUsername(name, str) {
                var error = "";
                var illegalChars = /\W/;
                if (str === "") {
                    error = "Vui lòng nhập " + name;
                } else if ((str.length < 6) || (str.length > 15)) {
                    error = name + " phải từ 6 - 15 ký tự";
                } else if (illegalChars.test(str)) {
                    error = name + " không đúng định dạng";
                } else {
                    error = "";
                }
                return error;
            }

        </script>

    </body>
    <script src="<%=request.getContextPath()%>/landing4/assets/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/landing4/assets/js/popper.min.js"></script>
    <script src="<%=request.getContextPath()%>/landing4/assets/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/landing4/assets/js/main.js?v=1"></script>
    <script src="<%=request.getContextPath()%>/landing4/assets/js/coin.js"></script>
</html>