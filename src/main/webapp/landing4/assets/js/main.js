$(document).ready(function(){
	 var baseURL = "https://portal.bomtan.win";
	 var uriCapcha = "/api/Account/Captcha";
	 var uriRegist = "/api/Account/CreateAccount"
	 var token = "";

	//validate password length
	function inValidLength(str, min, max) {
		if(str.length < min || str.length > max) {
			return true;
		}
		return false;
	};

	//validate same
	function isSame(str1, str2) {
		return str1 === str2;
	};

	//validate empty
	function isEmpty(str){
		return str.length == 0
	}

	//function show message 
	function messageError(msg){
		$("#message").removeClass('success').addClass('error').text('').text(msg);
	};

	function getCapcha(){
	 	$.get(baseURL+uriCapcha, function(res){
		 	$("#privateKey").val(res[0]);
		 	$("#capcha").attr('src', "data:image/jpeg;base64,"+res[1]);
	 	});
	};
	
	
	
	 getCapcha();
	 
	 $("#refresh-capcha").click(function(event) {
	 	getCapcha();
	 });

	 $(".btn-regist").click(function(){
	 	var userName = $("#username").val();
	 	var password = $("#password-regist").val();
	 	var confirmPass = $("#confirm-password-regist").val();
	 	var capcha = $("#input-capcha").val();
	 	var privateKey = $("#privateKey").val();

	 	if(isEmpty(username)) {
	 		messageError('Bạn vui lòng nhập tài khoản!')
	 		return;
	 	}
	 	if(inValidLength(userName, 6, 15)) {
	 		$("#username").focus();
	 		messageError('Tài khoản phải từ 6-15 ký tự!');
	 		return;
	 	}

	 	if(isEmpty(password)) {
	 		$("#password-regist").focus();
	 		messageError('Bạn vui lòng nhập mật khẩu!')
	 		return;
	 	}

	 	if(inValidLength(password, 6, 18)) {
	 		$("#password-regist").focus();
	 		messageError('Mật khẩu phải từ 6-18 ký tự!');
	 		return;
	 	}

 		if(isEmpty(confirmPass)) {
	 		messageError('Bạn vui lòng nhập mật khẩu xác nhận!')
	 		$("#confirm-password-regist").focus();
	 		return;
	 	}

	 	if(!isSame(password, confirmPass)) {
	 		messageError('Mật khẩu xác nhận không khớp!');
	 		$("#confirm-password-regist").focus();
	 		return;
	 	}

	 	if(isEmpty(capcha)) {
	 		$("#input-capcha").focus();
	 		messageError('Bạn vui lòng nhập mã xác nhận!')
	 		return;
	 	}

	 	var inputRegist = {LoginType:1, UserName: userName, Password: password, DeviceId: 'web', DeviceType: 1, PrivateKey: privateKey, Captcha: capcha};
	 	 $.ajax({
            type: "POST",
            url: baseURL + uriRegist,
            data: JSON.stringify(inputRegist),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
			crossDomain: true,
            xhrFields: {
                 withCredentials: true
            },
            success: function (data) {
            	if(data.ResponseCode == 1) {
            		location.href = "https://bomtan.win/"
            	}else {
            		$("#message").removeClass('success').addClass('error').text(data.Message+"!")
            		getCapcha();
            	}
            },
            fail: function (fail) {
                console.log("Loi");
            }
        });
     
	 });


});